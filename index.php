<?php
  error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
  include_once('ruta.php');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-toto-fit=no">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="Punta Brava">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<title>Bodegón Punta Brava</title>

		<link rel="icon" href="static/img/logo.png">
		<link rel="stylesheet" href="static/css/bootstrap.min.css">
		<link rel="stylesheet" href="static/css/owl.carousel.css">
		<link rel="stylesheet" href="static/css/owl.theme.css">
		<link rel="stylesheet" href="static/css/animate.css">
		<link rel="stylesheet" href="static/css/magnific-popup.css">
		<link rel="stylesheet" href="static/css/nivo-lightbox.css">
		<link rel="stylesheet" href="static/css/main.css">    
		<link rel="stylesheet" href="static/css/estilos.css">    
		<link rel="stylesheet" href="static/css/responsive.css">
		<link rel="stylesheet" href="static/css/all.min.css">
		<link rel="stylesheet" href="static/css/flickity.css">


	</head>

	<body>
		<header id="home" class="header-home">
			<div class="overlay"></div>
			<div class="banner-top text-center mb-0">
				<b class="text-center"><i class="fa fa-map-marker"></i> Guanare, Venezuela | </b>
				<b class="text-center">Realiza tu pedido a través de Whatsapp <a href="https://api.whatsapp.com/send?phone=5804245106537&amp;text=Hola me gustaria realizar el siguiente pedido: " target="_blank" style="color: #fff;"> <i class="fab fa-whatsapp"></i> +58 4245106537</a></b>
			</div>

			<nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar" style="top: 25px !important;">
				<div class="container">
					<a href="index.php" class="navbar-brand">
						<img src="static/img/logo.png" alt="" class="img_brand">
					</a>  

					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
						<i class="fa fa-bars"></i>
					</button>

					<div class="collapse navbar-collapse" id="navbarCollapse">
						<ul class="navbar-nav mr-auto w-100 justify-content-end">
							<li class="nav-item">
								<a class="nav-link page-scroll" href="#home">INICIO</a>
							</li>
							<li class="nav-item">
								<a class="nav-link page-scroll" href="#nosotros">NOSOTROS</a>
							</li>  
							<li class="nav-item">
								<a class="nav-link page-scroll" href="#productos">PRODUCTOS</a>
							</li>                            
							<li class="nav-item">
								<a class="nav-link page-scroll" href="#contacto">CONTACTO</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>

		<?php include($ruta); ?>

		<footer>
			<section class="footer-Content">
				<div class="copyright">
					<div class="container">
						<div class="row">
							<div class="col-md-12 text-center">
								<div class="site-info text-center">
									<p class="m-0 text-bold">&copy; 2020 Todos los derechos reservados, desarrollado por <a href="https://pladecompany.com/">Plade Company C.A</a></p>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</footer>

		<a href="#" class="back-to-top">
			<i class="fa fa-arrow-up"></i>
		</a> 

		<div id="preloader">
			<div class="loader" id="loader-1"></div>
		</div>

		<script src="static/js/jquery-min.js"></script>
		<script src="static/js/popper.min.js"></script>
		<script src="static/js/bootstrap.min.js"></script>
		<script src="static/js/owl.carousel.js"></script> 
		<script src="static/js/jquery.mixitup.js"></script>       
		<script src="static/js/jquery.nav.js"></script>
		<script src="static/js/scrolling-nav.js"></script>        
		<script src="static/js/jquery.easing.min.js"></script>     
		<script src="static/js/wow.js"></script>   
		<script src="static/js/jquery.counterup.min.js"></script>    
		<script src="static/js/jquery.magnific-popup.min.js"></script>  
		<script src="static/js/nivo-lightbox.js"></script>
		<script src="static/js/main.js"></script>
		<script src="static/js/flickity.js"></script>

	</body>
</html>