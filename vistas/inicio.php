<div id="carouselPrincipal" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#carouselPrincipal" data-slide-to="0" class="active"></li>
		<li data-target="#carouselPrincipal" data-slide-to="1" class=""></li>
		<li data-target="#carouselPrincipal" data-slide-to="2" class=""></li>
	</ol>

	<div class="carousel-inner">
		<div class="carousel-item active">
			<img class="d-block w-100 hide-lp" alt="" src="static/img/puntabrava-slider-1.jpg" data-holder-rendered="true">
			<img class="d-block w-100 hide-mv" alt="" src="static/img/puntabrava-slidermv-1.jpg" data-holder-rendered="true">
			<div class="hero">
				<hgroup>
					<h1 class="text-center">Bodegones que ofrecen productos <br><b>frescos y atractivos</b></h1><br>
					<h3 class="text-center">Donde encontrarás un trato amable y productos de alta calidad siempre.</h3>

					<h3 class="text-center">Descubre todo lo que tenemos para tí</h3><br>

					<h3 class="text-center">Te invitamos a disfrutar la experiencia desde nuestra web,<br> solicita tus pedidos por <span style="color: #e74c3c; font-weight: bold;">WhatsApp (+58 4245106537)</span></h3>
				</hgroup>
				
				<div class="text-center">
					<a href="https://api.whatsapp.com/send?phone=5804245106537&amp;text=Hola me gustaria realizar el siguiente pedido: " target="_blank" class="btn btn-border-filled"><i class="fab fa-whatsapp"></i> Solicita tu pedido</a>
				</div>
			</div>
		</div>

		<div class="carousel-item">
			<img class="d-block w-100 hide-lp" alt="" src="static/img/puntabrava-slider-2.jpg" data-holder-rendered="true">
			<img class="d-block w-100 hide-mv" alt="" src="static/img/puntabrava-slidermv-2.jpg" data-holder-rendered="true">
			<div class="hero">
				<hgroup>
					<h1 class="text-center">Servirte es una prioridad</h1><br>
					<h3 class="text-center">Si necesitas información no dudes en preguntar</h3>
				</hgroup>
				
				<div class="text-center">
					<a href="https://api.whatsapp.com/send?phone=5804245106537&amp;text=Hola me gustaria realizar el siguiente pedido: " target="_blank" class="btn btn-border-filled"><i class="fab fa-whatsapp"></i> Consúltanos</a>
				</div>
			</div>
		</div>
		
		<div class="carousel-item">
			<img class="d-block w-100 hide-lp" alt="" src="static/img/puntabrava-slider-3.jpg" data-holder-rendered="true">
			<img class="d-block w-100 hide-mv" alt="" src="static/img/puntabrava-slidermv-3.jpg" data-holder-rendered="true">
			<div class="hero">
				<hgroup>
					<h1 class="text-center">¡Te lo llevamos hasta tu casa!</h1><br>
					<h3 class="text-center">Contamos con servicio de Delivery en todo Guanare</h3>
					<h3 class="text-center">Para más información</h3>
				</hgroup>
				
				<div class="text-center">
					<a href="https://api.whatsapp.com/send?phone=5804245106537&amp;text=Hola me gustaria realizar el siguiente pedido: " target="_blank" class="btn btn-border-filled"><i class="fab fa-whatsapp"></i> Contáctanos</a>
				</div>
			</div>
		</div>
	</div>

	<a class="carousel-control-prev" href="#carouselPrincipal" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>

	<a class="carousel-control-next" href="#carouselPrincipal" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
</div>


<div class="p-3 app-features">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-6">
				<div class="text-center">
					<div class="box-item center">
						<img src="static/img/productos/pro-carne.jpg" class="w-100 img-productos">
						<div class="text pt-2">
							<h4>Res y Parrillada</h4>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-6">
				<div class="text-center">
					<div class="box-item center">
						<img src="static/img/productos/pro-cochino.jpg" class="w-100 img-productos">
						<div class="text pt-2">
							<h4>Cerdo y Otras Carnes</h4>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-6">
				<div class="text-center">
					<div class="box-item center">
						<img src="static/img/productos/pro-pollo.jpg" class="w-100 img-productos">
						<div class="text pt-2">
							<h4>Pollos</h4>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-6">
				<div class="text-center">
					<div class="box-item center">
						<img src="static/img/productos/pro-chacuteria.jpg" class="w-100 img-productos">
						<div class="text pt-2">
							<h4>Charcutería</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="nosotros" class="section">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-12 col-sm-12">
				<div class="img-thumb wow fadeInLeft" data-wow-delay="0.3s">
					<img class="img-fluid" src="static/img/sobre-nosotros.jpg" alt=""  style="border-radius: .5rem;">
				</div>
			</div>
			<div class="col-lg-6 col-md-12 col-sm-12">
				<div class="text-wrapper wow fadeInRight" data-wow-delay="0.6s">
					<div>
						<p class="btn btn-subtitle">Sobre nosotros</p>  
						<h5 class="mb-3">Bodegón Punta Brava por años brindando su servicio a la población Guanareña. Contamos con una sucursal recién inaugurada, la cual mantiene los estándares de calidad del primero.</h5>

						<h5>Ofrecemos excelentes productos, entre los que destacan nuestra variedad en carnes, además disponemos de un servicio centrado en el cliente y garantías sanitarias.</h5>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt">
			<div class="col-lg-6 col-md-12 col-sm-12">
				<div class="text-wrapper wow fadeInRight" data-wow-delay="0.9s">
					<div>
						<p class="btn btn-subtitle hide-lp">¿Por qué comprar en Bodegón Punta Brava?</p> 
						<p class="btn btn-subtitle hide-mv">¿Por qué comprar en <br> Bodegón Punta Brava?</p> 
						<h5 class="mb-3">En <b>Bodegón Punta Brava</b> te brindamos variedad de productos y un servicio de calidad.</h5>

						<h5>Facilitamos tu compra, mediante la opción de solicitar su pedido desde la comodidad de su hogar, lo cual le permite ahorrar tiempo y disfrutar de una experiencia de compra incomparable.</h5>

						<h5><b>Servicio de delivery</b> <i class="fa fa-angle-right"></i> Ofrecemos servicio de delivery según la zona, consultar precios por <a href="https://api.whatsapp.com/send?phone=5804245106537&amp;text=Hola me gustaria realizar el siguiente pedido: " target="_blank"><b>WhatsApp (+58 4245106537)</b></a></h5>
					</div>
				</div>
			</div>
			 <div class="col-lg-6 col-md-12 col-sm-12">
				<div class="img-thumb wow fadeInLeft" data-wow-delay="1.2s">
					<img class="img-fluid" src="static/img/nuestros-productos.jpg" alt="" style="border-radius: .5rem;">
				</div>
			</div>
		</div>
	</div>
</div>


<div class="section app-features">
	<div class="container">
		<div class="section-header">    
			<h2 class="section-title wow fadeIn" data-wow-delay="0.2s">Compra rápida</h2>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-12 col-xs-12">
				<div class="content-left text-right">
					<div class="box-item left">
						<span class="icon">
							<i class="fa fa-shopping-basket"></i>
						</span>
						<div class="text">
							<h4>PASO 1</h4>
							<p>Selecciona los productos que deseas comprar</p>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-12 col-xs-12">
				<div class="content-left text-right">
					<div class="box-item left">
						<span class="icon">
							<i class="fab fa-whatsapp"></i>
						</span>
						<div class="text">
							<h4>PASO 2</h4>
							<p>Píde a través de WhatsApp</p>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-12 col-xs-12">
				<div class="content-left text-right">
					<div class="box-item left">
						<span class="icon">
							<i class="fa fa-id-card"></i>
						</span>
						<div class="text">
							<h4>PASO 3</h4>
							<p>Realiza tu pago y envíanos el comprobante</p>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-12 col-xs-12">
				<div class="content-left text-right">
					<div class="box-item left">
						<span class="icon">
							<i class="fa fa-motorcycle"></i>
						</span>
						<div class="text">
							<h4>PASO 4</h4>
							<p>Te lo enviamos a tu casa</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="productos" class="section carniceria">
	<div class="section-header">   
		<p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Carnicería</p>       
		<h2 class="section-title">Res, Cerdo y Pollo</h2>
	</div>
	
	<div class="productos main-carousel" data-flickity='{ "cellAlign": "left", "contain": true }'>
		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/carne.jpg" class="img-productos" alt="Carne">
				<h6 class="text-bold">Carne</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/cochino.jpg" class="img-productos" alt="Cochino">
				<h6 class="text-bold">Cochino</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/pollo.jpg" class="img-productos" alt="Pollo">
				<h6 class="text-bold">Pollo</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/pollo.jpg" class="img-productos" alt="Pollo">
				<h6 class="text-bold">Carne</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/pollo.jpg" class="img-productos" alt="Pollo">
				<h6 class="text-bold">Carne</h6>
			</div>
		</div>
	</div>
</div>


<div class="section charcuteria">
	<div class="section-header">   
		<p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Charcutería</p>       
		<h2 class="section-title">Queso, mortadela, tocineta, jamón...</h2>
	</div>
	
	<div class="productos main-carousel" data-flickity='{ "cellAlign": "left", "contain": true }'>
		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/mortadela.jpg" class="img-productos" alt="Mortadela">
				<h6 class="text-bold">Mortadela</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/queso.jpg" class="img-productos" alt="Queso">
				<h6 class="text-bold">Queso</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/tocineta.jpg" class="img-productos" alt="Tocineta">
				<h6 class="text-bold">Tocineta</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/jamon.jpg" class="img-productos" alt="jamon">
				<h6 class="text-bold">Jamón</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/jamon.jpg" class="img-productos" alt="jamon">
				<h6 class="text-bold">Jamón</h6>
			</div>
		</div>
	</div>
</div>


<div class="section viveres">
	<div class="section-header">   
		<p class="btn btn-subtitle wow fadeInDown" data-wow-delay="0.2s">Víveres</p>       
		<h2 class="section-title">Café, Salsas, Cubitos, Harina y mucho más</h2>
	</div>
	
	<div class="productos main-carousel" data-flickity='{ "cellAlign": "left", "contain": true }'>
		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/v1.jpg" class="img-productos" alt="Harina">
				<h6 class="text-bold">Harina</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/v2.jpg" class="img-productos" alt="Vinagre">
				<h6 class="text-bold">Vinagre</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/v3.jpg" class="img-productos" alt="Cubitos">
				<h6 class="text-bold">Cubitos</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/v4.jpg" class="img-productos" alt="Salsa de tomate">
				<h6 class="text-bold">Salsa de tomate</h6>
			</div>
		</div>

		<div class='carousel-cell'>
			<div class="card-productos">
				<img src="static/img/productos/v4.jpg" class="img-productos" alt="Salsa de tomate">
				<h6 class="text-bold">Salsa de tomate</h6>
			</div>
		</div>
	</div>
</div>

<section class="section video-promo section">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
					<div class="video-promo-content text-center">
						<a href="https://www.youtube.com/watch?v=yP6kdOZHids" class="video-popup"><i class="fa fa-play"></i></a>
						<h2 class="mt-3 wow zoomIn" data-wow-duration="1000ms" data-wow-delay="100ms">Ver Video</h2>
					</div>
			</div>
		</div>
	</div>
</section>


<section id="contacto" class="section">    
	<div class="section-header">
		<h2 class="section-title wow fadeIn" data-wow-delay="0.2s">Contáctanos</h2>
		<h5 class="mb-2 text-center">Disponemos de un canal de comunicación cómodo y rápido para tí</h5>
	</div>  

	<div class="row">
		<div class="col-sm-12 col-md-6">
			<div class="row">
				<div class="col-md-12">
					<h5 class="mb-3 text-center">Solicita tu pedido o información escribiéndonos al <br><b><a href="https://api.whatsapp.com/send?phone=5804245106537&amp;text=Hola me gustaria realizar el siguiente pedido: " target="_blank"><b>WhatsApp (+58 4245106537)</b></a></b></h5>

					<div class="row mb-3">
						<div class="col-sm-12 col-md-6 text-center">
							<div class="content-left p-1 text-center">
								<div class="box-item left">
									<span class="icon">
										<i class="fa fa-phone"></i>
									</span>
									<div class="text">
										<h4 class="cont-subtitle">TELÉFONO</h4>
										<p><a href="https://api.whatsapp.com/send?phone=5804245106537&amp;text=Hola me gustaria realizar el siguiente pedido: " target="_blank"><b>+58 4245106537</b></a></p>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-12 col-md-6">
							<div class="content-left p-1 text-center">
								<div class="box-item left">
									<span class="icon">
										<i class="fab fa-instagram"></i>
									</span>
									<div class="text">
										<h4 class="cont-subtitle">SÍGUENOS</h4>
										<p><a href="https://www.instagram.com/bodegonpuntabrava/" target="_blank"><b>@bodegonpuntabrava</b></a></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12">
					<h5 class="mb-3 text-center">Visítanos en cualquiera de nuestras sucursales y disfruta de nuestro servicio.</h5>

					<div class="row mb-3">
						<div class="col-6 wow fadeInLeft" data-wow-delay="0.3s">
							<div class="content-left p-1 text-center">
								<div class="box-item left">
									<span class="icon">
										<i class="fa fa-route"></i>
									</span>
									<div class="text">
										<h4 class="cont-subtitle">SUCURSAL</h4>
										<p>Carrera 6 entre calles 11 y 12, al lado de CORPOELEC<br> Venezuela - Portuguesa, Guanare</p>
									</div>
								</div>
							</div>
						</div>

						<div class="col-6 wow fadeInLeft" data-wow-delay="0.3s">
							<div class="content-left p-1 text-center">
								<div class="box-item left">
									<span class="icon">
										<i class="fa fa-route"></i>
									</span>
									<div class="text">
										<h4 class="cont-subtitle">DIRECCIÓN</h4>
										<p>Carrera 11 con calle 9,<br> Venezuela - Portuguesa, Guanare</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<img src="static/img/contacto.jpg" width="100%">
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-12 col-md-6">
			<div class="powr-social-feed" id="08104543_1595429043"></div>
			<script src="https://www.powr.io/powr.js?platform=embed"></script>
			</div>
		</div>
	</div>
</section>

